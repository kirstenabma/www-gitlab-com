---
layout: markdown_page
title: Jenkins GitLab integration
---

Please see the [GitLab Jenkins integration documentation](https://docs.gitlab.com/ee/integration/jenkins.html).